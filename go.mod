module gitee.com/fabric-gm/fabric-contract-api-go

go 1.14

require (
	gitee.com/fabric-gm/fabric-chaincode-go v0.0.0-20210907055832-060bc6ed2bef
	github.com/go-openapi/spec v0.20.3
	github.com/gobuffalo/packr v1.30.1
	github.com/hyperledger/fabric-protos-go v0.0.0-20210720123151-f0dc3e2a0871
	github.com/xeipuuv/gojsonschema v1.2.0
)
